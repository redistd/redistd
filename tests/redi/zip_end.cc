// Copyright Jonathan Wakely 2012
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <redi/zip.h>

#include <iterator>
#include <cassert>

struct container
{
  struct iterator
  {
    typedef std::forward_iterator_tag   iterator_category;
    typedef int                         value_type;
    typedef int*                        pointer;
    typedef int&                        reference;
    typedef std::ptrdiff_t              difference_type;

    int n;
    static unsigned count;

    iterator& operator++() { ++n; ++count; return *this; }

    bool operator==(iterator const& i) { return n == i.n; }
    bool operator!=(iterator const& i) { return n != i.n; }
  };

  iterator begin() { return iterator{0}; }
  iterator end() { return iterator{n}; }

  int n;
};

unsigned container::iterator::count = 0;

// test that zipper::end() doesn't need to increment iterators
void t1()
{
  auto z1 = redi::zip(container{3}, container{3});
  container::iterator::count = 0;
  z1.end();
  assert( container::iterator::count == 0 );

  auto z2 = redi::zip(container{3}, container{4});
  container::iterator::count = 0;
  z2.end();
  assert( container::iterator::count == 2*z2.size() );

  auto z3 = redi::zip(container{4}, container{3});
  container::iterator::count = 0;
  z3.end();
  assert( container::iterator::count == 2 * z3.size() );
}

int main()
{
  t1();
}

// vi: set ft=cpp sw=2:
