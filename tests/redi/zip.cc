// Copyright Jonathan Wakely 2012
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <redi/zip.h>

#include <string>
#include <vector>
#include <list>
#include <map>
#include <iterator>
#include <sstream>
#include <cassert>

std::vector<int> vi() { return std::vector<int>{ 0, 1, 2, 999 }; }

void t1()
{
  std::vector<std::string> vs{ "a", "b", "c" };

  std::ostringstream ss;
  for (auto&& i : redi::zip(vi(), vs))
    ss << i.get<0>() << ' ' << i.get<1>() << ' ';
  assert( ss.str() == "0 a 1 b 2 c " );
}

void t2()
{
  std::list<int> l{ 0, 1, -1, -1 };
  std::map<std::string, std::string> m{ { "A", "a" }, { "B", "b" } };
  std::string s{ "abcdef" };
  int a[] = { 0, 1, -1, -2 };

  std::ostringstream ss;
  for (auto&& i : redi::zip(l, m, s, a))
    ss << i.get<0>() << i.get<1>().first << i.get<1>().second
      << i.get<2>() << i.get<3>();
  assert( ss.str() == "0Aaa01Bbb1" );
}

void t3()
{
  auto z1 = redi::zip(std::string("abc"), std::string("def"));
  assert( z1.size() == 3 );
  assert( std::distance(z1.begin(), z1.end()) == 3 );

  auto z2 = redi::zip(std::string("abc"), std::string("defg"));
  assert( z2.size() == 3 );
  assert( std::distance(z2.begin(), z2.end()) == 3 );
}

void t4()
{
  const int ca[] = { 0, 1, -1, -2 };
  int a[sizeof(ca)];
  std::copy(std::begin(ca), std::end(ca), std::begin(a));
  std::list<int> l{ std::begin(ca), std::end(ca) };
  const std::list<int> cl = l;

  std::ostringstream ss;
  for (auto&& i : redi::zip(l, cl, a, ca))
  {
    assert( i.get<0>() == i.get<1>() );
    assert( i.get<0>() == i.get<2>() );
    assert( i.get<0>() == i.get<3>() );
  }
}

int main()
{
  t1();
  t2();
  t3();
  t4();
}

// vi: set ft=cpp sw=2:
