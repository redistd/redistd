#include "redi/posix_mutex.h"

#include <thread>
#include <atomic>
#include <cassert>
#include <cstdlib>

int main()
{
  redi::robust_mutex m;
  std::atomic_bool go { false };

  std::thread t( [&] { m.lock(); go = true; while (go) { } } );
  while (!go)
  { }
  assert( !m.try_lock() );
  go = false;
  try
  {
    m.lock();
    std::abort();
  }
  catch (std::system_error const& e)
  {
    if (e.code() != make_error_code(std::errc::state_not_recoverable))
      throw;
  }
  assert( !m.try_lock() );
  t.join();
}

