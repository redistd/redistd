// Copyright Jonathan Wakely 2013
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <redi/zip.h>

#include <boost/range/adaptor/map.hpp>

#include <forward_list>
#include <map>
#include <iterator>
#include <sstream>
#include <cassert>

std::forward_list<int> fi() { return std::forward_list<int>{ 0, 1, 2, 999 }; }

// test with ForwardIterators and boost::iterator_range
void t1()
{
  std::map<std::string, int> ms{ {"a",0}, {"b",0}, {"c",0} };

  std::ostringstream ss;
  for (auto&& i : redi::zip(fi(), ms | boost::adaptors::map_keys))
    ss << i.get<0>() << ' ' << i.get<1>() << ' ';
  auto sss = ss.str();
  assert( ss.str() == "0 a 1 b 2 c " );
}

int main()
{
  t1();
}

// vi: set ft=cpp sw=2:
