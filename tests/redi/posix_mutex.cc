#include <redi/posix_mutex.h>
#include <mutex>
#include <cassert>

using redi::posix_mutex;

static_assert(std::is_standard_layout<posix_mutex>::value, "standard layout");

int main()
{
  posix_mutex m;
  bool locked = m.try_lock();
  assert( locked );
  assert( !m.try_lock() );
  m.unlock();

  {
    std::lock_guard<posix_mutex> lock(m);
    assert(!m.try_lock());
  }
  {
    std::lock_guard<posix_mutex> lock(m);
    assert(!m.try_lock());
  }
}

