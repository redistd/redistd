// Copyright Jonathan Wakely 2013
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <redi/not_fn.h>

#include <cassert>

struct A
{
  bool b = false;
  bool get() const {return b;}
  bool operator()() const {return b;}
};

bool f() { return false; }
bool f2(bool b1, bool b2) { return b1 && b2; }

int main()
{
  using redi::not_fn;

  assert( not_fn( []{return false;} )() );
  assert( not_fn( [](bool b) {return b;} )(false) );
  assert( not_fn( f )() );
  assert( not_fn( f2 )(false, true) );
  assert( not_fn( &A::b )( A{} ) );
  assert( not_fn( &A::get )( A{} ) );
  assert( not_fn( A{} )() );
}

// vi: set ft=cpp sw=2:
