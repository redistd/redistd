#include <redi/exec_around.h>

struct Foo
{
  int f() { return 0 ; }
  int g() const { return 1; }
  int h(int hh) { return hh; }
  int i;
};

int main()
{
  auto foo = redi::mutex_around<Foo>(3);
  const auto& cfoo = foo;
  auto f = foo->f();
  auto g = cfoo->g();
  auto h = foo->h(2);
  auto i = cfoo->i;
  return f + g + h - i;
}
