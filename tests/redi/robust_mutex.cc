#include "redi/posix_mutex.h"

#include <exception>
#include <new>
#include <system_error>
#include <string>

#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

using redi::pshared_mutex;
using redi::pshared_cond_var;

namespace {

struct Error : std::system_error
{
    Error(char const* where)
    : std::system_error(errno, std::generic_category(), where)
    { }

    Error(char const* where, const char* filename)
    : std::system_error(errno, std::generic_category(), std::string(where)+"(\""+filename+"\")")
    { }
};

struct SharedData
{
    pshared_mutex mtx_1;
    pshared_mutex mtx_2;
    pshared_cond_var cnd;
    unsigned long event;

    SharedData()
        : mtx_1(pshared_mutex::prio::inherit)
        , mtx_2(pshared_mutex::prio::inherit)
        , event()
    { }

    static SharedData* map(char const* filename, unsigned pid)
    {
        int fd = open(filename, O_CREAT | O_RDWR, mode_t(0666));
        if(fd < 0)
            throw Error("open", filename);
        struct stat st;
        if(fstat(fd, &st))
            throw Error("fstat", filename);
        bool new_file = !st.st_size;
        if(new_file) {
            if(ftruncate(fd, sizeof(SharedData)))
              throw Error("ftruncate", filename);
        }
        void* mem = mmap(NULL, sizeof(SharedData), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        close(fd);
        if(MAP_FAILED == mem)
            throw Error("mmap", filename);
        if(new_file)
            new (mem) SharedData;
        printf("%u: opened %s file\n", pid, new_file ? "new" : "existing");
        return static_cast<SharedData*>(mem);
    }
};

char const shared_file[] = "shared_file_test~";

pid_t spawn(int(*fn)())
{
    // fork a child process
    pid_t pid = fork();
    switch(pid) {
    case 0:
        exit(fn());
    case -1:
        abort();
    default:
        return pid;
    }
}

int process_1()
{
    unsigned pid = getpid();
    printf("%u: process 1\n", pid);

    SharedData* shared_data = SharedData::map(shared_file, pid);
    shared_data->mtx_1.lock(); // abandon this mutex, EOWNERDEAD in pthread_cond_wait
    shared_data->mtx_2.lock(); // abandon this mutex, EOWNERDEAD in pthread_mutex_lock
    shared_data->event = 1;
    shared_data->cnd.notify_one();
    printf("%u: terminated\n", pid);

    return 0;
}

int process_2()
{
    unsigned pid = getpid();
    printf("%u: process 2\n", pid);

    SharedData* shared_data = SharedData::map(shared_file, pid);
    printf("%u: locking shared data\n", pid);

    int failures = 0;

    printf("%u: locking mtx_1...\n", pid);
    std::unique_lock<pshared_mutex> l1(shared_data->mtx_1);
    if (l1.consistent())
    {
        ++failures;
        fprintf(stderr, "check failed: mtx_1 is not reported as inconsistent");
    }
    else
        l1.recover();
    printf("%u: mtx_1 locked\n", pid);

    while(1 != shared_data->event)
        shared_data->cnd.wait(l1);

    printf("%u: locking mtx_2...\n", pid);
    std::unique_lock<pshared_mutex> l2(shared_data->mtx_2);
    if (l2.consistent())
    {
        ++failures;
        fprintf(stderr, "check failed: mtx_2 is not reported as inconsistent");
    }
    else
        l2.recover();
    printf("%u: mtx_2 locked\n", pid);

    return failures ? EXIT_FAILURE : EXIT_SUCCESS;
}

}

int main(int ac, char** av)
{
    unsigned mode = ac > 1 ? std::stoul(av[1]) : 0;
    switch(mode) {
    case 0: {
        // start with a new file
        unlink(shared_file);
        // fork process_1 and wait till it terminates
        pid_t child = spawn(process_1);
        int child_status;
        if(-1 == waitpid(child, &child_status, 0))
            throw Error("waitpid");

        // now do process_2
        return process_2();
    }

    case 1:
        // start with a new file
        unlink(shared_file);
        // run only process 1
        return process_1();

    case 2:
        // start with an existing file with abandoned mutexes
        // run only process 2
        return process_2();
    }
}
