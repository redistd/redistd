#include <redi/move_only_function.h>

#include <utility>
#include <cassert>

using redi::move_only_function;

using std::is_same_v;
using std::is_invocable_v;
using std::is_nothrow_invocable_v;
using std::invoke_result_t;

using std::is_constructible_v;
using std::is_copy_constructible_v;
using std::is_nothrow_default_constructible_v;
using std::is_nothrow_move_constructible_v;
using std::is_nothrow_constructible_v;
using std::nullptr_t;
using std::in_place_type_t;

static_assert( is_nothrow_default_constructible_v<move_only_function<void()>> );
static_assert( is_nothrow_constructible_v<move_only_function<void()>, nullptr_t> );
static_assert( is_nothrow_move_constructible_v<move_only_function<void()>> );
static_assert( ! is_copy_constructible_v<move_only_function<void()>> );

static_assert( is_constructible_v<move_only_function<void()>, void()> );
static_assert( is_constructible_v<move_only_function<void()>, void(&)()> );
static_assert( is_constructible_v<move_only_function<void()>, void(*)()> );
static_assert( is_constructible_v<move_only_function<void()>, int()> );
static_assert( is_constructible_v<move_only_function<void()>, int(&)()> );
static_assert( is_constructible_v<move_only_function<void()>, int(*)()> );
static_assert( ! is_constructible_v<move_only_function<void()>, void(int)> );
static_assert( is_constructible_v<move_only_function<void(int)>, void(int)> );

static_assert( is_constructible_v<move_only_function<void(int)>,
				  in_place_type_t<void(*)(int)>, void(int)> );

static_assert( is_constructible_v<move_only_function<void()>,
				  void() noexcept> );
static_assert( is_constructible_v<move_only_function<void() noexcept>,
				  void() noexcept> );
static_assert( ! is_constructible_v<move_only_function<void() noexcept>,
				    void() > );

struct Q
{
  void operator()() const &;
  void operator()() &&;
};

static_assert( is_constructible_v<move_only_function<void()>, Q> );
static_assert( is_constructible_v<move_only_function<void() const>, Q> );
static_assert( is_constructible_v<move_only_function<void() &>, Q> );
static_assert( is_constructible_v<move_only_function<void() const &>, Q> );
static_assert( is_constructible_v<move_only_function<void() &&>, Q> );
static_assert( is_constructible_v<move_only_function<void() const &&>, Q> );

struct R
{
  void operator()() &;
  void operator()() &&;
};

static_assert( is_constructible_v<move_only_function<void()>, R> );
static_assert( is_constructible_v<move_only_function<void()&>, R> );
static_assert( is_constructible_v<move_only_function<void()&&>, R> );
static_assert( ! is_constructible_v<move_only_function<void() const>, R> );
static_assert( ! is_constructible_v<move_only_function<void() const&>, R> );
static_assert( ! is_constructible_v<move_only_function<void() const&&>, R> );

// The following nothrow-constructible guarantees are an extension,
// not required by the standard.

static_assert( is_nothrow_constructible_v<move_only_function<void()>, void()> );
static_assert( is_nothrow_constructible_v<move_only_function<void(int)>,
					  in_place_type_t<void(*)(int)>,
					  void(int)> );

// These types are all small and nothrow move constructible
struct F { void operator()(); };
struct G { void operator()() const; };
static_assert( is_nothrow_constructible_v<move_only_function<void()>, F> );
static_assert( is_nothrow_constructible_v<move_only_function<void()>, G> );
static_assert( is_nothrow_constructible_v<move_only_function<void() const>, G> );

struct H {
  H(int);
  H(int, int) noexcept;
  void operator()() noexcept;
};
static_assert( is_nothrow_constructible_v<move_only_function<void()>, H> );
static_assert( is_nothrow_constructible_v<move_only_function<void() noexcept>,
					  H> );
static_assert( ! is_nothrow_constructible_v<move_only_function<void() noexcept>,
					    in_place_type_t<H>, int> );
static_assert( is_nothrow_constructible_v<move_only_function<void() noexcept>,
					  in_place_type_t<H>, int, int> );
// Check return types
static_assert( is_same_v<void, invoke_result_t<move_only_function<void()>>> );
static_assert( is_same_v<int, invoke_result_t<move_only_function<int()>>> );
static_assert( is_same_v<int&, invoke_result_t<move_only_function<int&()>>> );

// With const qualifier
static_assert( ! is_invocable_v< move_only_function<void()> const > );
static_assert( ! is_invocable_v< move_only_function<void()> const &> );
static_assert( is_invocable_v< move_only_function<void() const> > );
static_assert( is_invocable_v< move_only_function<void() const> &> );
static_assert( is_invocable_v< move_only_function<void() const> const > );
static_assert( is_invocable_v< move_only_function<void() const> const &> );

// With no ref-qualifier
static_assert( is_invocable_v< move_only_function<void()> > );
static_assert( is_invocable_v< move_only_function<void()> &> );
static_assert( is_invocable_v< move_only_function<void() const> > );
static_assert( is_invocable_v< move_only_function<void() const> &> );
static_assert( is_invocable_v< move_only_function<void() const> const > );
static_assert( is_invocable_v< move_only_function<void() const> const &> );

// With & ref-qualifier
static_assert( ! is_invocable_v< move_only_function<void()&> > );
static_assert( is_invocable_v< move_only_function<void()&> &> );
static_assert( is_invocable_v< move_only_function<void() const&> > );
static_assert( is_invocable_v< move_only_function<void() const&> &> );
static_assert( is_invocable_v< move_only_function<void() const&> const > );
static_assert( is_invocable_v< move_only_function<void() const&> const &> );

// With && ref-qualifier
static_assert( is_invocable_v< move_only_function<void()&&> > );
static_assert( ! is_invocable_v< move_only_function<void()&&> &> );
static_assert( is_invocable_v< move_only_function<void() const&&> > );
static_assert( ! is_invocable_v< move_only_function<void() const&&> &> );
static_assert( is_invocable_v< move_only_function<void() const&&> const > );
static_assert( ! is_invocable_v< move_only_function<void() const&&> const &> );

// With noexcept-specifier
static_assert( ! is_nothrow_invocable_v< move_only_function<void()> > );
static_assert( ! is_nothrow_invocable_v< move_only_function<void() noexcept(false)> > );
static_assert( is_nothrow_invocable_v< move_only_function<void() noexcept> > );
static_assert( is_nothrow_invocable_v< move_only_function<void()& noexcept>& > );

void
test01()
{
  struct F
  {
    int operator()() { return 0; }
    int operator()() const { return 1; }
  };

  move_only_function<int()> f0{F{}};
  assert( f0() == 0 );
  assert( std::move(f0)() == 0 );

  move_only_function<int() const> f1{F{}};
  assert( f1() == 1 );
  assert( std::as_const(f1)() == 1 );
  assert( std::move(f1)() == 1 );
  assert( std::move(std::as_const(f1))() == 1 );

  move_only_function<int()&> f2{F{}};
  assert( f2() == 0 );
  // Not rvalue-callable: std::move(f2)()

  move_only_function<int() const&> f3{F{}};
  assert( f3() == 1 );
  assert( std::as_const(f3)() == 1 );
  assert( std::move(f3)() == 1 );
  assert( std::move(std::as_const(f3))() == 1 );

  move_only_function<int()&&> f4{F{}};
  // Not lvalue-callable: f4()
  assert( std::move(f4)() == 0 );

  move_only_function<int() const&&> f5{F{}};
  // Not lvalue-callable: f5()
  assert( std::move(f5)() == 1 );
  assert( std::move(std::as_const(f5))() == 1 );
}

void
test02()
{
  struct F
  {
    int operator()() & { return 0; }
    int operator()() && { return 1; }
  };

  move_only_function<int()> f0{F{}};
  assert( f0() == 0 );
  assert( std::move(f0)() == 0 );

  move_only_function<int()&&> f1{F{}};
  // Not lvalue callable: f1()
  assert( std::move(f1)() == 1 );

  move_only_function<int()&> f2{F{}};
  assert( f2() == 0 );
  // Not rvalue-callable: std::move(f2)()
}

void
test03()
{
  struct F
  {
    int operator()() const & { return 0; }
    int operator()() && { return 1; }
  };

  move_only_function<int()> f0{F{}};
  assert( f0() == 0 );
  assert( std::move(f0)() == 0 );

  move_only_function<int()&&> f1{F{}};
  // Not lvalue callable: f1()
  assert( std::move(f1)() == 1 );

  move_only_function<int() const> f2{F{}};
  assert( f2() == 0 );
  assert( std::as_const(f2)() == 0 );
  assert( std::move(f2)() == 0 );
  assert( std::move(std::as_const(f2))() == 0 );

  move_only_function<int() const &&> f3{F{}};
  // Not lvalue callable: f3()
  assert( std::move(f3)() == 0 );
  assert( std::move(std::as_const(f3))() == 0 );

  move_only_function<int() const &> f4{F{}};
  assert( f4() == 0 );
  assert( std::as_const(f4)() == 0 );
  // Not rvalue-callable: std::move(f4)()
}

void
test04()
{
  struct F
  {
    int operator()() & { return 0; }
    int operator()() && { return 1; }
    int operator()() const & { return 2; }
    int operator()() const && { return 3; }
  };

  move_only_function<int()> f0{F{}};
  assert( f0() == 0 );
  assert( std::move(f0)() == 0 );

  move_only_function<int()&> f1{F{}};
  assert( f1() == 0 );
  // Not rvalue-callable: std::move(f1)()

  move_only_function<int()&&> f2{F{}};
  // Not lvalue callable: f2()
  assert( std::move(f2)() == 1 );

  move_only_function<int() const> f3{F{}};
  assert( f3() == 2 );
  assert( std::as_const(f3)() == 2 );
  assert( std::move(f3)() == 2 );
  assert( std::move(std::as_const(f3))() == 2 );

  move_only_function<int() const &> f4{F{}};
  assert( f4() == 2 );
  assert( std::as_const(f4)() == 2 );
  // Not rvalue-callable: std::move(f4)()

  move_only_function<int() const &&> f5{F{}};
  // Not lvalue callable: f5()
  assert( std::move(f5)() == 3 );
  assert( std::move(std::as_const(f5))() == 3 );
}

void
test05()
{
  // Small type with non-throwing move constructor. Not allocated on the heap.
  struct F
  {
    F() = default;
    F(const F& f) : counters(f.counters) { ++counters.copy; }
    F(F&& f) noexcept : counters(f.counters) { ++counters.move; }

    F& operator=(F&&) = delete;

    struct Counters
    {
      int copy = 0;
      int move = 0;
    } counters;

    Counters operator()() const noexcept { return counters; }
  };

  F f;
  move_only_function<F::Counters() const> m1(f);
  assert( m1().copy == 1 );
  assert( m1().move == 0 );

  // This will move construct a new target object and destroy the old one:
  auto m2 = std::move(m1);
  assert( m1 == nullptr && m2 != nullptr );
  assert( m2().copy == 1 );
  assert( m2().move == 1 );

  m1 = std::move(m2);
  assert( m1 != nullptr && m2 == nullptr );
  assert( m1().copy == 1 );
  assert( m1().move == 2 );

  m2 = std::move(f);
  assert( m2().copy == 0 );
  assert( m2().move == 2 ); // move construct target object, then swap into place
  const int moves = m1().move + m2().move;
  // This will do three moves:
  swap(m1, m2);
  assert( m1().copy == 0 );
  assert( m2().copy == 1 );
  assert( (m1().move + m2().move) == (moves + 3) );
}

void
test06()
{
  // Move constructor is potentially throwing. Allocated on the heap.
  struct F
  {
    F() = default;
    F(const F& f) noexcept : counters(f.counters) { ++counters.copy; }
    F(F&& f) noexcept(false) : counters(f.counters) { ++counters.move; }

    F& operator=(F&&) = delete;

    struct Counters
    {
      int copy = 0;
      int move = 0;
    } counters;

    const Counters& operator()() const { return counters; }
  };

  F f;
  move_only_function<const F::Counters&() const> m1(f);
  assert( m1().copy == 1 );
  assert( m1().move == 0 );

  // The target object is on the heap so this just moves a pointer:
  auto m2 = std::move(m1);
  assert( m1 == nullptr && m2 != nullptr );
  assert( m2().copy == 1 );
  assert( m2().move == 0 );

  m1 = std::move(m2);
  assert( m1 != nullptr && m2 == nullptr );
  assert( m1().copy == 1 );
  assert( m1().move == 0 );

  m2 = std::move(f);
  assert( m2().copy == 0 );
  assert( m2().move == 1 );
  const int moves = m1().move + m2().move;
  // This just swaps the pointers, so no moves:
  swap(m1, m2);
  assert( m1().copy == 0 );
  assert( m2().copy == 1 );
  assert( (m1().move + m2().move) == moves );
}

int main()
{
  test01();
  test02();
  test03();
  test04();
  test05();
  test06();
}
