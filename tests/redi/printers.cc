#include <vector>
#include <utility>
#include <redi/printers.h>

namespace x = redi;

struct A
{
    std::vector<int> v_;

    A() : v_{1, 2, 3} {}
};

std::ostream&
print_one(std::ostream& os, const A& a)
{
    using namespace x;
    return print_one(os, a.v_);
}

std::ostream&
operator<<(std::ostream& os, const A& a)
{
    using namespace x;
    return print_one(os, a.v_);
}

int main()
{
    A a1;
    x::println(a1);
    std::swap(a1, a1);
    x::println(a1);
    std::vector<std::vector<A>> v3(3, std::vector<A>(2, a1));
    x::println(v3);
}

