// Copyright Jonathan Wakely 2012
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <redi/index_tuple.h>

#include <type_traits>

template<unsigned N, unsigned... Ind>
struct test
{
  typedef typename redi::make_index_tuple<N>::type      test_type;
  typedef redi::index_tuple<Ind...>                     expected;

  static_assert(std::is_same<test_type, expected>::value, "ok");
};

template struct test<0>;
template struct test<1, 0>;
template struct test<2, 0, 1>;
template struct test<3, 0, 1, 2>;
template struct test<4, 0, 1, 2, 3>;
template struct test<5, 0, 1, 2, 3, 4>;
template struct test<6, 0, 1, 2, 3, 4, 5>;
template struct test<7, 0, 1, 2, 3, 4, 5, 6>;

