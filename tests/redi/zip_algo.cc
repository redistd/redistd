// Copyright Jonathan Wakely 2012
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <redi/zip.h>

#include <algorithm>
#include <vector>
#include <list>
#include <map>

struct A {
  bool ready() { return true; }
};

struct B { };

struct C {
  void process(const B&) { }
};

int main()
{
  std::vector<A> a;
  std::list<B> b;
  std::map<int, C> c;

  auto z = redi::zip(a, b, c);
  typedef decltype(z)::iterator::reference reference;

  std::for_each(z.begin(), z.end(), [](reference i) {
      if (i.get<0>().ready())
        i.get<2>().second.process( i.get<1>() );
    });
}

