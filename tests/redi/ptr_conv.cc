// Copyright Jonathan Wakely 2013
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <redi/ptr_conv.h>

#include <cassert>

using namespace redi;
using namespace std;

void test_make_shared_ptr()
{
  auto up = unique_ptr<int>(new int());
  auto sp = make_shared_ptr(std::move(up));
  static_assert( is_same<decltype(sp), shared_ptr<int>>::value, "shared_ptr<int>" );
  assert( up == nullptr );
  assert( sp != nullptr );
  assert( sp.unique() );

  sp = make_shared_ptr(std::move(up));
  assert( sp == nullptr );

  struct null_deleter {
      void operator()(void*) const { }
  };
  long d = 0;
  auto upd = unique_ptr<long, null_deleter>(&d);
  auto spd = make_shared_ptr(std::move(upd));
  static_assert( is_same<decltype(spd), shared_ptr<long>>::value, "shared_ptr<long>" );
  assert( upd == nullptr );
  assert( spd != nullptr );
  assert( spd.unique() );
  assert( get_deleter<null_deleter>(spd) != nullptr );
}

void test_make_weak_ptr()
{
  auto sp = make_shared<int>(1);
  auto wp = make_weak_ptr(sp);
  static_assert( is_same<decltype(wp), weak_ptr<int>>::value, "weak_ptr<int>" );
  assert( sp.unique() );
  assert( !wp.expired() );
  assert( wp.lock() == sp );
}

int main()
{
  test_make_shared_ptr();
  test_make_weak_ptr();
}
