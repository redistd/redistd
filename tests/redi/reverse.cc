// Copyright Jonathan Wakely 2012
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <redi/reverse.h>

#include <sstream>
#include <cassert>

void t1()
{
  int array[] = { 0, 1, 2, 3, 4 };

  std::ostringstream ss;
  for (auto i : redi::reverse(array))
    ss << i << ' ';
  assert( ss.str() == "4 3 2 1 0 " );
}

int main()
{
  t1();
}

// vi: set ft=cpp sw=2:
