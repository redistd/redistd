// Copyright Jonathan Wakely 2012
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <redi/tidy_ptr.h>

#include <cassert>

using redi::tidy_ptr;

void test_construct()
{
  tidy_ptr<int> p0;
  assert( !static_cast<bool>(p0) );
  assert( !p0 );
  assert( p0 == p0 );
  assert( !(p0 != p0) );
  assert( p0 == nullptr );
  assert( nullptr == p0 );

  int i = 0;
  tidy_ptr<int> p1(&i);
  assert( static_cast<bool>(p1) );
  assert( p1 == p1 );
  assert( p1 != p0 );
  assert( p0 != p1 );
  assert( p1 != nullptr );
  assert( nullptr != p1 );
}

void test_assign()
{
  tidy_ptr<int> p0;

  int i = 0;
  p0 = &i;
  assert( p0 == &i );

  p0 = nullptr;
  assert( p0 == nullptr );
}

void test_copy(int* p = nullptr)
{
  tidy_ptr<int> p0(p);
  assert( p0 == p );
  assert( p == p0 );

  tidy_ptr<int> p1(p0);
  assert( p1 == p );
  assert( p1 == p0 );

  p1 = nullptr;
  p1 = p0;
  assert( p1 == p );
  assert( p1 == p0 );

  if (!p)
  {
    int i = 0;
    test_copy(&i);
  }
}

void test_move(int* p = nullptr)
{
  tidy_ptr<int> p0(p);
  tidy_ptr<int> p1(std::move(p0));
  assert( p1 == p );
  assert( p0 == nullptr );

  p0 = std::move(p1);
  assert( p0 == p );
  assert( p1 == nullptr );

  if (!p)
  {
    int i = 0;
    test_move(&i);
  }
}

void test_get()
{
  tidy_ptr<int> p0;
  assert( p0.get() == nullptr );
  assert( p0.get() == p0 );
  int i = 0;
  p0 = &i;
  assert( p0.get() == p0 );
  assert( p0.get() == &i );
}

void test_deref()
{
  int i = 0;
  tidy_ptr<int> p0(&i);
  assert( &*p0 == &i );

  struct A { int i = 0; } a;
  tidy_ptr<A> p1(&a);
  assert( &*p1 == &a );
  assert( &p1->i == &a.i );
}

void test_swap()
{
  int i = 0;
  tidy_ptr<int> p0(&i);
  tidy_ptr<int> p1;
  swap( p0, p1 );
  assert( p0 == nullptr );
  assert( p1 == &i );
}

void test_conv()
{
  tidy_ptr<int> p0;
  tidy_ptr<const int> p1;

  struct A { };
  struct B : A { };
  tidy_ptr<B> p2;
  tidy_ptr<A> p3(p2);
  assert( p2 == p3 );
  assert( p3 == p2 );
  assert( !(p2 != p3) );
  assert( !(p3 != p2) );
  p3 = p2;
}

int main()
{
  test_construct();
  test_assign();
  test_copy();
  test_move();
  test_get();
  test_deref();
  test_swap();
  test_conv();
}
