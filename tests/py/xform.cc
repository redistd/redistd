#include <boost/python.hpp>
#include <py/def_function.h>
#include <cctype>

std::function<char(char)> f = [](char c) { return std::toupper(c); };
char c = 'l';

BOOST_PYTHON_MODULE( xform ) {
  using namespace boost::python;
  py::def_function<char(char)>("char_xform_fun_t", "transform a character");
  scope().attr("f") = f;
  scope().attr("c") = c;
}

