#ifndef PY_DEF_FUNCTION_H
#define PY_DEF_FUNCTION_H

// Copyright Alcides Viamontes Esquivel 2008
// Copyright Jonathan Wakely 2011
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#if __cplusplus < 201103L && !defined(__GXX_EXPERIMENTAL_CXX0X__)
#error C++11 compiler required
#else

/******************************************************************************
 * How to wrap std::function objects with boost.python and use them both from
 * C++ and Python.
 * 
 * Based on py_boost_function.hpp written by Alcides Viamontes Esquivel
 * http://wiki.python.org/moin/boost.python/HowTo#boost.function_objects
 *
 ******************************************************************************
 *
 * Usage example:
 *
 *     For the boost.python C++ module:
 *     
 *     ...
 *     #include <boost/python.hpp>
 *     ...
 *     #include "py_std_function.hpp"
 *     ...
 *
 *     void module_greeter_f(std::string const& origin)
 *     {
 *           cout << "Hello world, by " << origin << endl;
 *     }
 *
 *
 *     std::function< void( std::string const& ) > module_greeter( 
 *          module_greeter_f
 *     ) ;
 *     ...
 *
 *     BOOST_PYTHON_MODULE( foo ) {
 *
 *          using namespace boost::python;
 *          ...
 *
 *          def_function< void(string const&) >(
 *              "greeter_function_t", 
 *              "A greeting function" 
 *          );
 *
 *          ...
 *          scope().attr("module_greeter") = module_greeter;
 *     }
 *
 * From python code:
 *
 *     - Invoke:
 *
 *          >>> import foo
 *          >>> foo.module_greeter("world")
 *
 *     - Create instances from python:
 *             
 *          >>> def my_greetings(hi):
 *          >>> ... print hi, ", world"
 *          >>> ...
 *          >>> grfunc = foo.greeter_function_t.from_callable( my_greetings )
 *
 */

#include <boost/python.hpp>
#include <functional>
#include <type_traits>

namespace py
{
  namespace detail
  {
    template <typename Signature>
      struct pyobject_invoker;

    template <typename Ret, typename... A>
      struct pyobject_invoker<Ret(A...)>
      {
        boost::python::object callable;

        template < typename... Arg >
          typename std::enable_if<sizeof...(A)==sizeof...(Arg), Ret>::type
          operator()(Arg&&... arg)
          {
            boost::python::object o = callable( std::forward<Arg>(arg)... );
            return boost::python::extract<Ret>( o );
          }
      };

    template <typename Func>
      std::function<Func> function_frompyobj( boost::python::object o )
      {
        return pyobject_invoker<Func>{ o };
      }

  } // namespace detail

  template <typename FT>
    void def_function(const char* func_name, const char* func_doc)
    {
      static_assert( std::is_function< FT >::value,
                     "Template argument must be a function type" ) ;
      namespace bp = boost::python;
      typedef std::function<FT> function_t;
      bp::class_< function_t >
        (func_name, func_doc, bp::no_init)
        .def("__call__", &function_t::operator() )
        .def("from_callable", &detail::function_frompyobj<FT> )
        .staticmethod("from_callable")
        ;
    }

} // namespace py

#endif // C++11

#endif // PY_DEF_FUNCTION_H

// vi: set ft=cpp sw=2:
