#ifndef REDI_NOT_FN_H
#define REDI_NOT_FN_H

// Copyright Jonathan Wakely 2013
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <functional>

namespace redi
{
  namespace detail
  {
    template<typename Fn>
      struct not_fn_type
      {
        Fn fn;

        template<typename... Args>
          auto
          operator()(Args&&... args)
          noexcept(noexcept(!fn(std::forward<Args>(args)...)))
          -> decltype(!fn(std::forward<Args>(args)...))
          {
            return !fn(std::forward<Args>(args)...);
          }
      };

    template<typename Fn>
      Fn
      make_callable(Fn f) { return f; }

    template<typename R, typename T>
      auto
      make_callable(R T::* f) -> decltype(std::mem_fn(f))
      { return std::mem_fn(f); }
  }

  // Create a simple call wrapper fn such that the expression
  // fn(a1, a2, ..., aN) is equivalent to !INVOKE(f, a1, a2, ..., aN).
  template<typename F>
    auto
    not_fn(F f) -> detail::not_fn_type<decltype(detail::make_callable(f))>
    {
      auto fn = detail::make_callable(f);
      return detail::not_fn_type<decltype(fn)>{fn};
    }

}  // namespace redi

#endif  // REDI_NOT_FN_H

// vi: set ft=cpp sw=2:
