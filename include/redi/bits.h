#ifndef REDI_BITS_H
#define REDI_BITS_H

// Copyright Jonathan Wakely 2015
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cstdint>

namespace redi
{
#if __cplusplus < 201402L
  namespace detail
  {
    constexpr std::uint32_t
    _rprop(std::uint32_t x, int n) { return x | (x >> n); }
  }
#endif

  /// Least power of 2 greater than or equal to x.
  constexpr std::uint32_t
  clp2(std::uint32_t x)
  {
#if __cplusplus >= 201402L
    // Algorithm from Hacker's Delight, Figure 3-3.
    x = x - 1;
    x = x | (x >> 1);
    x = x | (x >> 2);
    x = x | (x >> 4);
    x = x | (x >> 8);
    x = x | (x >>16);
    return x + 1;
#else
    using detail::_rprop;
    return _rprop(_rprop(_rprop(_rprop(_rprop(x - 1, 1), 2), 4), 8), 16) + 1;
#endif
  }

}
#endif
