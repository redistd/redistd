#ifndef REDI_REVERSE_H
#define REDI_REVERSE_H

// Copyright Jonathan Wakely 2012
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iterator>
#include <cstddef>

namespace redi
{
  template<typename T, std::size_t N>
    struct reversed_array
    {
        typedef std::reverse_iterator<T*> iterator;

        iterator begin() const noexcept { return iterator(array+N); }
        iterator end() const noexcept { return iterator(array); }

        T* array;
    };

  /// Object generator to allow traversing an array in reverse with range-for
  /**
   * int array[] = { 0, 1, 2, 3, 4 };
   * for (auto i : redi::reverse(array))
   *   std::cout << i << ' ';
   */
  template<typename T, std::size_t N>
    inline reversed_array<T, N>
    reverse(T (&a)[N]) noexcept
    { return reversed_array<T, N>{a}; }

}  // namespace redi

#endif  // REDI_REVERSE_H

// vi: set ft=cpp sw=2:
