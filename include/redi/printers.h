#ifndef REDI_PRINTERS_H
#define REDI_PRINTERS_H

// Copyright Jonathan Wakely 2012
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <string>
#include <iterator>
#include <iostream>
#include <type_traits>

/* template<typename... T> void println(const T&...);
 *
 * The function template println() writes any number of objects to
 * standard output then outputs a newline character.
 *
 * An object x can be written if for a std::ostream object, o, the expression
 *     o << x
 * or
 *     print_one(o, x)
 * is well-formed.
 */

namespace redi
{
  namespace detail
  {
    template<typename T, typename OstreamT>
      class is_stream_insertable_helper
      {
          template<typename T2,
                   typename = decltype(std::declval<OstreamT&>() << std::declval<T2>())>
          static std::true_type test(T2* t);

          static std::false_type test(...);

      public:
          typedef decltype(test(std::declval<T*>())) type;
      };

    template<typename T, typename OstreamT = std::ostream>
      struct is_stream_insertable : is_stream_insertable_helper<T, OstreamT>::type
      { };

    template<typename T>
      using EnableIfOstreamInsertable
        = typename std::enable_if<is_stream_insertable<T>::value>::type;

    template<typename T>
      using DisableIfOstreamInsertable
        = typename std::enable_if<!is_stream_insertable<T>::value>::type;

  } // namespace detail

  template<typename T, typename = detail::EnableIfOstreamInsertable<T>>
    inline
    std::ostream&
    print_one(std::ostream& os, const T& t)
    {
      os << t;
      return os;
    }

  template<typename Range, typename = detail::DisableIfOstreamInsertable<Range>,
           typename = decltype(*std::begin(std::declval<Range>()))>
    std::ostream&
    print_one(std::ostream& os, const Range& range)
    {
      os << '{';
      const char* sep = "";
      for (auto& e : range)
      {
        os << sep;
        print_one(os, e);
        sep = ", ";
      }
      os << '}';
      return os;
    }

  inline
  void
  println()
  { std::cout << std::endl; }

  template<typename T0, typename... T>
    inline
    void
    println(const T0& t0, const T&... t)
    {
      print_one(std::cout, t0);
      println(t...);
    }

} // namespace redi
#endif
