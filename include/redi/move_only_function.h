// Implementation of std::move_only_function -*- C++ -*-

// Copyright (C) 2021 Jonathan Wakely
//
// This file is part of the redistd library.  This library is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// Under Section 7 of GPL version 3, you are granted additional
// permissions described in the GCC Runtime Library Exception, version
// 3.1, as published by the Free Software Foundation.

// You should have received a copy of the GNU General Public License
// along with this program; see the file COPYING3.  If not, see
// <http://www.gnu.org/licenses/>.

#ifndef REDI_MOVE_ONLY_FUNCTION_H
#define REDI_MOVE_ONLY_FUNCTION_H 1

#ifdef __cpp_lib_move_only_function
namespace redi { using std::move_only_function; }
#elif __cplusplus >= 202002L

#include <functional> // std::invoke
#include <utility>    // in_place_type_t
#include <stdexcept>    // in_place_type_t

namespace redi
{
  using namespace std;

  template<typename... Signature>
    class move_only_function; // not defined

  class mofunc_base
  {
  protected:
    mofunc_base() noexcept
    : m_manage(s_empty)
    { }

    mofunc_base(mofunc_base&& x) noexcept
    {
      m_manage = std::exchange(x.m_manage, s_empty);
      m_manage(m_storage, &x.m_storage);
    }

    template<typename T, typename... Args>
      static constexpr bool
      s_nothrow_init() noexcept
      {
	if constexpr (stored_locally<T>)
	  return is_nothrow_constructible_v<T, Args...>;
	return false;
      }

    template<typename T, typename... Args>
      void
      m_init(Args&&... args) noexcept(s_nothrow_init<T, Args...>())
      {
	if constexpr (stored_locally<T>)
	  ::new (m_storage.addr()) T(std::forward<Args>(args)...);
	else
          m_storage.m_p = new T(std::forward<Args>(args)...);

        m_manage = &s_manage<T>;
      }

    mofunc_base&
    operator=(mofunc_base&& x) noexcept
    {
      m_manage(m_storage, nullptr);
      m_manage = std::exchange(x.m_manage, s_empty);
      m_manage(m_storage, &x.m_storage);
      return *this;
    }

    mofunc_base&
    operator=(nullptr_t) noexcept
    {
      m_manage(m_storage, nullptr);
      m_manage = s_empty;
      return *this;
    }

    ~mofunc_base() { m_manage(m_storage, nullptr); }

    void
    swap(mofunc_base& x) noexcept
    {
      Storage s;
      x.m_manage(s, &x.m_storage);
      m_manage(x.m_storage, &m_storage);
      x.m_manage(m_storage, &s);
      std::swap(m_manage, x.m_manage);
    }

    template<typename T, typename Self>
      static T*
      s_access(Self* self) noexcept
      {
        if constexpr (stored_locally<remove_const_t<T>>)
          return static_cast<T*>(self->m_storage.addr());
	else
	  return static_cast<T*>(self->m_storage.m_p);
      }

  private:
    struct Storage
    {
      void* addr() noexcept { return &m_bytes[0]; }
      const void* addr() const noexcept { return &m_bytes[0]; }

      // We want to have enough space to store a simple delegate type.
      struct Delegate { void (Storage::*pfm)(); Storage* obj; };
      union {
	void* m_p;
	alignas(Delegate) alignas(void(*)())
	  unsigned char m_bytes[sizeof(Delegate)];
      };
    };

    template<typename T>
      static constexpr bool stored_locally
	= sizeof(T) <= sizeof(Storage) && alignof(T) <= alignof(Storage)
	    && is_nothrow_move_constructible_v<T>;

    // A function that either destroys the target object stored in target,
    // or moves the target object from *src to target.
    using Manager = void (*)(Storage& target, Storage* src) noexcept;

    // The no-op manager function for objects with no target.
    static void s_empty(Storage&, Storage*) noexcept { }

    // The real manager function for a target object of type T.
    template<typename T>
      static void
      s_manage(Storage& target, Storage* src) noexcept
      {
	if constexpr (stored_locally<T>)
	  {
	    if (src)
	      {
                T* rval = static_cast<T*>(src->addr());
		::new (target.addr()) T(std::move(*rval));
		rval->~T();
	      }
	    else
	      static_cast<T*>(target.addr())->~T();
	  }
	else
	  {
	    if (src)
	      target.m_p = src->m_p;
	    else
	      delete static_cast<T*>(target.m_p);
	  }
      }

    Storage m_storage;
    Manager m_manage;
  };

  template<typename T>
    inline constexpr bool is_move_only_function_v = false;
  template<typename T>
    constexpr bool is_move_only_function_v<move_only_function<T>> = true;

  template<typename T>
    inline constexpr bool is_in_place_type_v = false;
  template<typename T>
    constexpr bool is_in_place_type_v<in_place_type_t<T>> = true;

} // namespace redi

#include "mofunc_impl.h"
#define REDI_MOF_CV const
#include "mofunc_impl.h"
#define REDI_MOF_REF &
#include "mofunc_impl.h"
#define REDI_MOF_REF &&
#include "mofunc_impl.h"
#define REDI_MOF_CV const
#define REDI_MOF_REF &
#include "mofunc_impl.h"
#define REDI_MOF_CV const
#define REDI_MOF_REF &&
#include "mofunc_impl.h"

#endif // C++20
#endif // REDI_MOVE_ONLY_FUNCTION_H
