#ifndef REDI_POSIX_MUTEX_H
#define REDI_POSIX_MUTEX_H

// Copyright Jonathan Wakely 2012
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <unistd.h>
#include <pthread.h>

#include <system_error>
#include <mutex>
#include <condition_variable>

// Determine if robust mutexes are supported
// TODO maybe replace with configure script
#if _POSIX_C_SOURCE >= 200809L || _XOPEN_SOURCE >= 700
# define REDI_PTHREAD_MUTEX_ROBUST PTHREAD_MUTEX_ROBUST
# define REDI_PTHREAD_MUTEX_STALLED PTHREAD_MUTEX_STALLED
# define REDI_PTHREAD_MUTEXATTR_SETROBUST pthread_mutexattr_setrobust
# define REDI_PTHREAD_MUTEX_CONSISTENT pthread_mutex_consistent
#elif defined(_GNU_SOURCE)
# define REDI_PTHREAD_MUTEX_ROBUST PTHREAD_MUTEX_ROBUST_NP
# define REDI_PTHREAD_MUTEX_STALLED PTHREAD_MUTEX_STALLED_NP
# define REDI_PTHREAD_MUTEXATTR_SETROBUST pthread_mutexattr_setrobust_np
# define REDI_PTHREAD_MUTEX_CONSISTENT pthread_mutex_consistent_np
#else
# define REDI_PTHREAD_MUTEX_ROBUST 0
# define REDI_PTHREAD_MUTEX_STALLED 0
#endif

#define REDI_CALL(A) pthread_call::wrap(#A, (A))

namespace redi
{
  class pthread_call
  {
    // Call wrapper for turning pthread API errors into exceptions
    template<typename... Args>
      struct call_wrapper
      {
        const char* where;
        int (*target)(Args...);

        void
        operator()(Args... args) const
        {
          if (int ev = target(args...))
            throw std::system_error(ev, std::generic_category(), where);
        }
      };

  public:
    template<typename... Args>
      static call_wrapper<Args...>
      wrap(const char* name, int (*f)(Args...)) noexcept
      { return call_wrapper<Args...>{name, f}; }
  };

  // Specialize for std::mutex if static assertion fails
  template<typename Mutex>
    struct native_mutex
    {
      static_assert( std::is_same<typename Mutex::native_handle_type,
                                  pthread_mutex_t*>::value,
                     "native_handle_type is pthread_mutex_t*" );

      pthread_mutex_t*
      operator()(Mutex& m) const noexcept { return m.native_handle(); }
    };

  // Specialize for std::condition_variable if static assertion fails
  template<typename CondVar>
    struct native_cv
    {
      static_assert( std::is_same<typename CondVar::native_handle_type,
                                  pthread_cond_t*>::value,
                     "native_handle_type is pthread_cond_t*" );

      pthread_cond_t*
      operator()(CondVar& cv) const noexcept { return cv.native_handle(); }
    };

  // Indicates whether a robust mutex was consistent when locked
  enum class robust_lock_result : char { locked, inconsistent, failed };

  // Mutex type supporting POSIX mutex attributes
  class posix_mutex : public std::mutex
  {
  public:
    enum type {
      normal = PTHREAD_MUTEX_NORMAL,
      errorcheck = PTHREAD_MUTEX_ERRORCHECK,
      recursive = PTHREAD_MUTEX_RECURSIVE
    };

    struct prio
    {
      enum ceiling : int { };

      enum protocol {
        none = PTHREAD_PRIO_NONE,
        inherit = PTHREAD_PRIO_INHERIT,
        protect = PTHREAD_PRIO_PROTECT
      };
    };

    enum pshared {
      process_private = PTHREAD_PROCESS_PRIVATE,
      process_shared = PTHREAD_PROCESS_SHARED
    };

    enum robust_t {
      stalled = REDI_PTHREAD_MUTEX_STALLED,
      robust = REDI_PTHREAD_MUTEX_ROBUST
    };

    constexpr posix_mutex() noexcept = default;

    template<typename... Attrs>
      explicit
      posix_mutex(Attrs... attrs);

    ~posix_mutex()
    { pthread_mutex_destroy(native_handle()); }

    void
    lock()
    { check(lock(robust)); }

    robust_lock_result
    lock(robust_t)
    {
      int e = pthread_mutex_lock(native_handle());
      auto res = lock_result(e);
      if (res == robust_lock_result::failed)
        throw std::system_error(e, std::generic_category());
      return res;
    }

    bool
    try_lock()
    { return check(try_lock(robust)); }

    robust_lock_result
    try_lock(robust_t)
    { return lock_result(pthread_mutex_trylock(native_handle())); }

#ifdef _POSIX_TIMEOUTS
    // Add timed mutex requirements to std::mutex

    template <class Rep, class Period>
      bool
      try_lock_for(const std::chrono::duration<Rep, Period>& rel_time);

    template <class Clock, class Duration>
      bool
      try_lock_until(const std::chrono::time_point<Clock, Duration>& abs_time);

    template <class Rep, class Period>
      robust_lock_result
      try_lock_for(const std::chrono::duration<Rep, Period>& rel_time,
                   robust_t);

    template <class Clock, class Duration>
      robust_lock_result
      try_lock_until(const std::chrono::time_point<Clock, Duration>& abs_time,
                     robust_t);
#endif

    // this just tells the app the state is OK again
    void
    recover()
    {
#ifdef REDI_PTHREAD_MUTEX_CONSISTENT
      pthread_call::wrap("pthread_mutex_consistent",
          REDI_PTHREAD_MUTEX_CONSISTENT)(native_handle());
#endif
    }

    typedef pthread_mutex_t* native_handle_type;

    pthread_mutex_t*
    native_handle() noexcept
    { return native_mutex<std::mutex>()(*this); }

  private:
    void
    set_attrs(pthread_mutexattr_t*) noexcept { }

    template<typename Attr, typename... Attrs>
      void
      set_attrs(pthread_mutexattr_t* a, Attr attr, Attrs... attrs)
      {
        set_attr(a, attr);
        set_attrs(a, attrs...);
      }

    void
    set_attr(pthread_mutexattr_t* attr, type t)
    { REDI_CALL(pthread_mutexattr_settype)(attr, t); }

    void
    set_attr(pthread_mutexattr_t* attr, prio::ceiling ceil)
    { REDI_CALL(pthread_mutexattr_setprioceiling)(attr, ceil); }

    void
    set_attr(pthread_mutexattr_t* attr, prio::protocol p)
    { REDI_CALL(pthread_mutexattr_setprotocol)(attr, p); }

    void
    set_attr(pthread_mutexattr_t* attr, pshared p)
    { REDI_CALL(pthread_mutexattr_setpshared)(attr, p); }

    void
    set_attr(pthread_mutexattr_t* attr, robust_t r)
    {
#ifdef REDI_PTHREAD_MUTEXATTR_SETROBUST
      REDI_CALL(REDI_PTHREAD_MUTEXATTR_SETROBUST)(attr, r);
#else
      throw std::system_error(make_error_code(std::errc::invalid_argument));
#endif
    }

    robust_lock_result
    lock_result(int e) noexcept
    {
      switch (e)
      {
      case 0:
        return robust_lock_result::locked;
      case EOWNERDEAD:
        return robust_lock_result::inconsistent;
      default:
        return robust_lock_result::failed;
      }
    }

    bool
    check(robust_lock_result res)
    {
      if (res == robust_lock_result::inconsistent)
      {
        unlock();
        throw std::system_error(make_error_code(std::errc::state_not_recoverable));
      }
      return res == robust_lock_result::locked;
    }
  };

  constexpr posix_mutex::robust_t robust = posix_mutex::robust;

  template<typename... Attrs>
    inline
    posix_mutex::posix_mutex(Attrs... attrs) : std::mutex()
    {
      pthread_mutexattr_t attr;
      REDI_CALL(pthread_mutexattr_init)(&attr);
      set_attrs(&attr, attrs...);
      REDI_CALL(pthread_mutex_init)(native_handle(), &attr);
      pthread_mutexattr_destroy(&attr);
    }

#ifdef _POSIX_TIMEOUTS
  template <class Rep, class Period>
    inline bool
    posix_mutex::
    try_lock_for(const std::chrono::duration<Rep, Period>& rel_time)
    {
      return check(try_lock_for(rel_time, robust));
    }

  template <class Clock, class Duration>
    inline bool
    posix_mutex::
    try_lock_until(const std::chrono::time_point<Clock, Duration>& abs_time)
    {
      return check(try_lock_until(abs_time, robust));
    }

  template <class Rep, class Period>
    inline robust_lock_result
    posix_mutex::
    try_lock_for(const std::chrono::duration<Rep, Period>& rel_time,
                 robust_t t)
    {
      using namespace std;
      using namespace chrono;
      using clock = typename conditional<high_resolution_clock::is_steady,
                                         high_resolution_clock,
                                         steady_clock>::type;
      using clock_tick = typename clock::duration;
      using timeout_tick = duration<Rep, Period>;
      auto clock_ticks = duration_cast<clock_tick>(rel_time);
      if (duration_cast<timeout_tick>(clock_ticks) != rel_time)
        ++clock_ticks;
      return try_lock_until(clock::now() + clock_ticks, t);
    }

  template <class Clock, class Duration>
    inline robust_lock_result
    posix_mutex::
    try_lock_until(const std::chrono::time_point<Clock, Duration>& abs_time,
                   robust_t)
    {
      using namespace std::chrono;
      auto tp_secs = time_point_cast<seconds>(abs_time);
      time_t sec = tp_secs.time_since_epoch().count();
      long nsec = duration_cast<nanoseconds>(abs_time - tp_secs).count();
      const timespec ts{ sec, nsec };
      return lock_result(pthread_mutex_timedlock(native_handle(), &ts));
    }
#endif

  struct robust_mutex : posix_mutex
  {
    template<typename... Attrs>
      explicit
      robust_mutex(Attrs... attrs)
      : posix_mutex(std::forward<Attrs>(attrs)..., posix_mutex::robust)
      { }
  };

  struct recursive_robust_mutex : robust_mutex
  {
    template<typename... Attrs>
      explicit
      recursive_robust_mutex(Attrs... attrs)
      : robust_mutex(std::forward<Attrs>(attrs)..., posix_mutex::recursive)
      { }
  };

  struct pshared_mutex : robust_mutex
  {
    template<typename... Attrs>
      explicit
      pshared_mutex(Attrs... attrs)
      : robust_mutex(std::forward<Attrs>(attrs)..., posix_mutex::process_shared)
      { }
  };

  struct recursive_pshared_mutex : pshared_mutex
  {
    template<typename... Attrs>
      explicit
      recursive_pshared_mutex(Attrs... attrs)
      : pshared_mutex(std::forward<Attrs>(attrs)..., posix_mutex::recursive)
      { }
  };

  class robust_lock
  {
    friend class unique_lock;
    friend class lock_guard;

    bool consistent = true;

    robust_lock() = default;

    explicit
    robust_lock(posix_mutex& m)
    : consistent(m.lock(robust) != robust_lock_result::inconsistent)
    { }

    std::unique_lock<std::mutex>
    try_to_lock(posix_mutex& m)
    {
      auto res = m.try_lock(robust);
      if (res == robust_lock_result::failed)
        return std::unique_lock<std::mutex>(m, std::defer_lock);
      consistent = res == robust_lock_result::locked;
      return std::unique_lock<std::mutex>(m, std::adopt_lock);
    }

    template <class Rep, class Period>
      std::unique_lock<std::mutex>
      try_to_lock_for(posix_mutex& m,
                      const std::chrono::duration<Rep, Period>& rel_time)
      {
        auto res = m.try_lock_for(rel_time, robust);
        if (res == robust_lock_result::failed)
          return std::unique_lock<std::mutex>(m, std::defer_lock);
        consistent = res == robust_lock_result::locked;
        return std::unique_lock<std::mutex>(m, std::adopt_lock);
      }

    template <class Clock, class Duration>
      std::unique_lock<std::mutex>
      try_to_lock_until(posix_mutex& m,
                        const std::chrono::time_point<Clock, Duration>& abs_time)
      {
        auto res = m.try_lock_until(abs_time, robust);
        if (res == robust_lock_result::failed)
          return std::unique_lock<std::mutex>(m, std::defer_lock);
        consistent = res == robust_lock_result::locked;
        return std::unique_lock<std::mutex>(m, std::adopt_lock);
      }
  };

  class lock_guard : private robust_lock, public std::lock_guard<std::mutex>
  {
    typedef std::lock_guard<std::mutex> base_type;
    posix_mutex& mx;

  public:
    explicit
    lock_guard(posix_mutex& m)
    : robust_lock(m), base_type(m, std::adopt_lock), mx(m)
    { }

    lock_guard(posix_mutex& m, std::adopt_lock_t a) : base_type(m, a), mx(m)
    { }

    void
    recover()
    {
      mx.recover();
      robust_lock::consistent = true;
    }

    bool
    consistent() const noexcept { return robust_lock::consistent; }
  };

  class unique_lock : public robust_lock, public std::unique_lock<std::mutex>
  {
    typedef std::unique_lock<std::mutex> base_type;

  public:
    typedef posix_mutex mutex_type;

    unique_lock() = default;

    explicit
    unique_lock(posix_mutex& m) : robust_lock(m), base_type(m, std::adopt_lock)
    { }

    unique_lock(posix_mutex& m, std::adopt_lock_t t) : base_type(m, t)
    { }

    unique_lock(posix_mutex& m, std::defer_lock_t t) : base_type(m, t)
    { }

    unique_lock(posix_mutex& m, std::try_to_lock_t)
    : base_type(try_to_lock(m))
    { }

    template <class Rep, class Period>
      unique_lock(posix_mutex& m,
                  const std::chrono::duration<Rep, Period>& rel_time)
      : base_type(try_to_lock_for(m, rel_time))
      { }

    template <class Clock, class Duration>
      unique_lock(posix_mutex& m,
                  const std::chrono::time_point<Clock, Duration>& abs_time)
      : base_type(try_to_lock_until(m, abs_time))
      { }

    void
    swap(unique_lock& l) noexcept
    {
      base_type::swap(static_cast<base_type&>(l));
      std::swap(robust_lock::consistent, l.robust_lock::consistent);
    }

    posix_mutex*
    mutex() const noexcept
    { return static_cast<posix_mutex*>(base_type::mutex()); }

    posix_mutex*
    release() noexcept
    { return static_cast<posix_mutex*>(base_type::release()); }

    void
    recover()
    {
      mutex()->recover();
      robust_lock::consistent = true;
    }

    bool
    consistent() const noexcept { return robust_lock::consistent; }
  };

  // std::condition_variable with the process-shared attribute
  struct pshared_cond_var : std::condition_variable
  {
    pshared_cond_var() : std::condition_variable()
    {
      pthread_condattr_t attr;
      REDI_CALL(pthread_condattr_init)(&attr);
      REDI_CALL(pthread_condattr_setpshared)(&attr, PTHREAD_PROCESS_SHARED);
      REDI_CALL(pthread_cond_init)(native_handle(), &attr);
      REDI_CALL(pthread_condattr_destroy)(&attr);
    }

    ~pshared_cond_var()
    { pthread_cond_destroy(native_handle()); }

    typedef pthread_cond_t* native_handle_type;

    pthread_cond_t*
    native_handle()
    { return native_cv<std::condition_variable>()(*this); }
  };

}  // namespace redi

namespace std
{
// TODO use inheriting constructors
#define REDI_LOCK_GUARD(M) \
  template<> \
    struct lock_guard<M> : redi::lock_guard \
    { \
      typedef redi::lock_guard base_type; \
      template<typename Arg> \
        using Constructible = std::is_constructible<base_type, M&, Arg>; \
      template<typename Arg> \
        using Valid = typename std::enable_if<Constructible<Arg>::value>::type; \
    public: \
      explicit lock_guard(M& m) : base_type(m) { } \
      template<typename Arg, typename = Valid<Arg>> \
        lock_guard(M& m, Arg arg) : base_type(m, arg) { } \
    }

#define REDI_UNIQ_LOCK(M) \
  template<> \
    class unique_lock<M> : public redi::unique_lock \
    { \
    private: \
      typedef redi::unique_lock base_type; \
      using base_type::swap; \
      using base_type::mutex; \
      using base_type::release; \
    public: \
      typedef M mutex_type; \
      unique_lock() = default; \
      explicit \
      unique_lock(M& m) : base_type(m) \
      { } \
      unique_lock(M& m, std::adopt_lock_t t) : base_type(m, t) \
      { } \
      unique_lock(M& m, std::defer_lock_t t) : base_type(m, t) \
      { } \
      unique_lock(M& m, std::try_to_lock_t t) : base_type(m, t) \
      { } \
      M* mutex() const noexcept { return static_cast<M*>(base_type::mutex()); } \
      M* release() noexcept { return static_cast<M*>(base_type::release()); } \
    }

  REDI_LOCK_GUARD(redi::posix_mutex);
  REDI_LOCK_GUARD(redi::robust_mutex);
  REDI_LOCK_GUARD(redi::recursive_robust_mutex);
  REDI_LOCK_GUARD(redi::pshared_mutex);
  REDI_LOCK_GUARD(redi::recursive_pshared_mutex);

  REDI_UNIQ_LOCK(redi::posix_mutex);
  REDI_UNIQ_LOCK(redi::robust_mutex);
  REDI_UNIQ_LOCK(redi::recursive_robust_mutex);
  REDI_UNIQ_LOCK(redi::pshared_mutex);
  REDI_UNIQ_LOCK(redi::recursive_pshared_mutex);

#undef REDI_LOCK_GUARD
#undef REDI_UNIQ_LOCK

}  // namespace std

#undef REDI_CALL

#endif  // REDI_POSIX_MUTEX_H

// vi: set ft=cpp sw=2:
