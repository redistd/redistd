#ifndef REDI_PTR_CONV_H
#define REDI_PTR_CONV_H

// Copyright Jonathan Wakely 2013
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <memory>

namespace redi
{
  /** Convert a unique_ptr to a shared_ptr
   *
   * For convenience when using 'auto'
   *
   * auto up = make_unique<X>();
   * auto sp = make_shared_ptr(up);
   *
   * If an exception is thrown this function has no effect.
   */
  template<typename T, typename D>
    inline std::shared_ptr<T>
    make_shared_ptr(std::unique_ptr<T, D>&& up)
    {
      return std::shared_ptr<T>(std::move(up));
    }

  /** Convert a shared_ptr to weak_ptr
   *
   * For convenience when using 'auto'
   *
   * auto sp = make_shared<X>();
   * auto wp = make_weak_ptr(sp);
   */
  template<typename T>
    inline std::weak_ptr<T>
    make_weak_ptr(const std::shared_ptr<T>& sp) noexcept
    {
      return std::weak_ptr<T>(sp);
    }

}  // namespace redi

#endif // REDI_PTR_CONV_H

// vi: set ft=cpp sw=2:
