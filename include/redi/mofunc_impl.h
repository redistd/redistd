// Implementation of std::move_only_function -*- C++ -*-

// Copyright (C) 2021 Jonathan Wakely
//
// This file is part of the redistd library.  This library is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// Under Section 7 of GPL version 3, you are granted additional
// permissions described in the GCC Runtime Library Exception, version
// 3.1, as published by the Free Software Foundation.

// You should have received a copy of the GNU General Public License
// along with this program; see the file COPYING3.  If not, see
// <http://www.gnu.org/licenses/>.

#ifndef REDI_MOF_CV
# define REDI_MOF_CV
#endif

#ifdef REDI_MOF_REF
# define REDI_MOF_INV_QUALS REDI_MOF_CV REDI_MOF_REF
#else
# define REDI_MOF_REF
# define REDI_MOF_INV_QUALS REDI_MOF_CV &
#endif

#define REDI_MOF_CV_REF REDI_MOF_CV REDI_MOF_REF

namespace redi
{
  template<typename Res, typename... ArgTypes, bool Noex>
    class move_only_function<Res(ArgTypes...) REDI_MOF_CV
                              REDI_MOF_REF noexcept(Noex)>
    : mofunc_base
    {
      template<typename T>
	using callable
	  = conditional_t<Noex,
                          is_nothrow_invocable_r<Res, T, ArgTypes...>,
                          is_invocable_r<Res, T, ArgTypes...>>;

      // [func.wrap.mov.con]/1 is-callable-from<VT>
      template<typename Vt>
	static constexpr bool is_callable_from
	  = conjunction_v<callable<Vt REDI_MOF_CV_REF>,
                          callable<Vt REDI_MOF_INV_QUALS>>;

    public:
      using result_type = Res;

      move_only_function() noexcept { }

      move_only_function(nullptr_t) noexcept { }

      move_only_function(move_only_function&& x) noexcept
      : mofunc_base(static_cast<mofunc_base&&>(x)),
        m_invoke(std::exchange(x.m_invoke, nullptr))
      { }

      template<typename F, typename Vt = decay_t<F>>
	requires (!is_same_v<Vt, move_only_function>)
	  && (!is_in_place_type_v<Vt>) && is_callable_from<Vt>
	move_only_function(F&& f) noexcept(s_nothrow_init<Vt, F>())
	{
	  if constexpr (is_function_v<remove_pointer_t<Vt>>
		        || is_member_pointer_v<Vt>
		        || is_move_only_function_v<Vt>)
	    {
	      if (f == nullptr)
		return;
	    }
          m_init<Vt>(std::forward<F>(f));
          m_invoke = &s_invoke<Vt>;
	}

      template<typename T, typename... Args>
	requires is_constructible_v<T, Args...>
	  && is_callable_from<T>
	explicit
	move_only_function(in_place_type_t<T>, Args&&... args)
	noexcept(s_nothrow_init<T, Args...>())
	: m_invoke(&s_invoke<T>)
	{
	  static_assert(is_same_v<decay_t<T>, T>);
          m_init<T>(std::forward<Args>(args)...);
	}

      template<typename T, typename U, typename... Args>
	requires is_constructible_v<T, initializer_list<U>&, Args...>
	  && is_callable_from<T>
	explicit
	move_only_function(in_place_type_t<T>, initializer_list<U> il,
			   Args&&... args)
	noexcept(s_nothrow_init<T, initializer_list<U>&, Args...>())
	: mofunc_base(nullptr), m_invoke(&s_invoke<T>)
	{
	  static_assert(is_same_v<decay_t<T>, T>);
          m_init<T>(il, std::forward<Args>(args)...);
	}

      move_only_function&
      operator=(move_only_function&& x) noexcept
      {
	mofunc_base::operator=(static_cast<mofunc_base&&>(x));
        m_invoke = std::exchange(x.m_invoke, nullptr);
	return *this;
      }

      move_only_function&
      operator=(nullptr_t) noexcept
      {
	mofunc_base::operator=(nullptr);
        m_invoke = nullptr;
	return *this;
      }

      template<typename F>
	requires is_constructible_v<move_only_function, F>
	move_only_function&
	operator=(F&& f)
	noexcept(is_nothrow_constructible_v<move_only_function, F>)
	{
	  move_only_function(std::forward<F>(f)).swap(*this);
	  return *this;
	}

      ~move_only_function() = default;

      explicit operator bool() const noexcept { return m_invoke != nullptr; }

      Res
      operator()(ArgTypes... args) REDI_MOF_CV_REF noexcept(Noex)
      {
        if (m_invoke == nullptr)
          throw std::logic_error("empty move_only_function");
	return m_invoke(this, std::forward<ArgTypes>(args)...);
      }

      void
      swap(move_only_function& x) noexcept
      {
	mofunc_base::swap(x);
	std::swap(m_invoke, x.m_invoke);
      }

      friend void
      swap(move_only_function& x, move_only_function& y) noexcept
      { x.swap(y); }

      friend bool
      operator==(const move_only_function& x, nullptr_t) noexcept
      { return x.m_invoke == nullptr; }

    private:
      template<typename T>
	using param_t
	  = conditional_t<is_trivially_copyable_v<T>
                            && sizeof(T) <= sizeof(long),
                          T, T&&>;

      using Invoker = Res (*)(mofunc_base REDI_MOF_CV*,
                              param_t<ArgTypes>...) noexcept(Noex);

      template<typename T>
	static Res
        s_invoke(mofunc_base REDI_MOF_CV* self,
		  param_t<ArgTypes>... args) noexcept(Noex)
	{
	  using Tcv = T REDI_MOF_CV;
	  using Tinv = T REDI_MOF_INV_QUALS;
          if constexpr (is_void_v<Res>)
            std::invoke(std::forward<Tinv>(*s_access<Tcv>(self)),
                        std::forward<param_t<ArgTypes>>(args)...);
          else
            return std::invoke(std::forward<Tinv>(*s_access<Tcv>(self)),
                               std::forward<param_t<ArgTypes>>(args)...);
	}

      Invoker m_invoke = nullptr;
    };

#undef REDI_MOF_CV_REF
#undef REDI_MOF_CV
#undef REDI_MOF_REF
#undef REDI_MOF_INV_QUALS

} // namespace redi
