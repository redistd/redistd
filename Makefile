check clean:
	$(MAKE) -C tests/redi $@
	$(MAKE) -C tests/py $@

test_%:
	$(MAKE) -C tests/redi $@

.PHONY: check clean
